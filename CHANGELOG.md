# CHANGELOG



## v1.4.0 (2024-05-29)

### Feature

* feat(yellow): adds info of which step was modified (#29) ([`e40e250`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/e40e250856cf803daa377ca71bbc9d9bb307db54))


## v1.3.0 (2024-05-22)

### Documentation

* docs: fix typo in README ([`3ed3932`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/3ed39326909a3325e59d241a8a280271653dc9ad))

### Feature

* feat: adds LV default front panel color to yellow (#6) ([`183d8e9`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/183d8e9f1c558d60b9c3f8af9ebf67b9cc1fa528))

* feat: adds move labels in yellow (#6) ([`bacc91f`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/bacc91f0307ae77f8368700ebcd01472fa3fdb4b))

* feat: adds auto grow off in structures in yellow (#6) ([`09de2e0`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/09de2e0381f4c31746dd4cb3355fc4ad11c65464))

* feat: adds application context to hooks (#28)

Besides this, also do some adjustments in windows appearance ([`59abe53`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/59abe5366880b1c35a40c20b4ddbf9295a552783))

### Fix

* fix: keep VI version when modifying a VI (#26)

Testing needed to remove fixtures from library as they are copied to a
temporary location, their library link gets broken and save for previous
do not work. This could be solved inside the test but it would
complicate the setup. ([`a8a20b4`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/a8a20b43c2f4c326e7a57b48d532e01b732b2345))

* fix: issue with malformed string in hook (#27) ([`6dc80d3`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/6dc80d354d7363683c5ebf67dd0d5a58dbd52b96))


## v1.2.0 (2024-04-07)

### Documentation

* docs: adds readme for mutation history #16 ([`66c6f5a`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/66c6f5a06b06f624b4baacfa2d40c234387671cb))

* docs: updates README with typo in the skip-fp option for yellow (#13)

Closes #13 ([`6062956`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/60629560b27f84a8fc53efb24a634584cc739735))

* docs: adds example file ([`38fa997`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/38fa997d32cb9497fde3e458e0611426d85ba797))

* docs: update hooks docs in README ([`f1f2951`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/f1f295106e7f262b60da271685a84e99393f0df1))

### Feature

* feat(vi-version): adds support for new hook #3 ([`01ff6a9`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/01ff6a9a9ad151adfbb79877ecd0019046bf786a))

* feat(check-missing-items): adds support for new hook #11 ([`d843b71`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/d843b71f8326d02bb9422eb13dd221b6898db53e))

* feat(separate-compiled-code): adds new hook #15 ([`349912e`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/349912e815138dc8192f27d2c94f8a5ef62c7181))

* feat: adds hook for removing mutation history #16 ([`c79e66d`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/c79e66dc2b9b3630d65d340a8d761b913763d870))

* feat: tests and documentation for discard no changes hook #5 ([`defa171`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/defa171f351ab93cbb831c193455b787e7766c6b))

* feat: adds hook for discarding changes when lvcompare returns no changes #5 ([`d660c1b`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/d660c1bd3bcbb833ea04daa29b8a574db6978b64))

* feat: adds dragon file and separate compiled code in all #18

Except the test fixture that needs to have it to be tested properly. ([`8af8cae`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/8af8cae52f2e6e2ae785c7698fd5b23c44592c84))

* feat: adds close labview hook ([`8b2cedc`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/8b2cedce17356635f12ae92420ba48f945bcbf06))

### Fix

* fix(yellow): workaround for bug in arrange windows #17 ([`bb46c0f`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/bb46c0f87c7c026fbb9a1c4861de12d0bc9bec00))

* fix(vi-broken): adds possibility to skip using bookmark #23 ([`05c4eb3`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/05c4eb391deb129216d9eb1657f0627727d387e6))

* fix: handles paths with spaces and improve cmd error #5 ([`1ec6bb0`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/1ec6bb0a58518b398f7c4fa13c5d02d19be7ba79))

* fix: adds condition to skip new vis in compare changes hook #5 ([`51cb912`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/51cb9128423ebccbc55664b9a5dda34a74c3db34))

* fix: adds option to hide loading box #24 ([`19514d0`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/19514d0e5ac65fb2a721895abb6faf31b15e2350))

* fix: adds exec cmd for wrapping systemexec and errors ([`3f10e6f`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/3f10e6f2d0630f3c393f1186990d3cb616e10637))

* fix(yellow): removes unused code ([`5372ea8`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/5372ea8ec309f8d28a069d50a8a73a8500ec8fc4))

* fix(ci): removes extra commit messages from changelog #20 ([`d5fde92`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/d5fde92d377ca781ee62547b999ef8fd5ea0f4d7))

* fix(ci): updates pylint command to point to right place #22

Closes #22 ([`67bfd6a`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/67bfd6a58ee5d8b1e1738e56d0240762a1f20194))

* fix: updates tests to comply with new editor and saving to lv 17 #18 ([`1c7bfe3`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/1c7bfe3fdb3dee50784056a33aa63ba712678800))

* fix: reverts file as this needs to have old version and compiled code ([`3c37ffd`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/3c37ffda511add2e82ccc4cd1ee030a3d32977cf))

### Test

* test: adds VI for testing VI versions #18 ([`70cdf7e`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/70cdf7e64a0b909209559d5d852466fe1c31a90c))


## v1.1.0 (2024-01-11)

### Feature

* feat: adds relative path description to the vi-broken hook (#2) ([`8cb25d5`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/8cb25d574ce65bfc41d183d7d3bfca75ee065a49))

* feat: adds relative path in yellow CLI output (#2) ([`ae658e5`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/ae658e599bafcf92a86fb2b1332d01e606d02a55))

* feat: adds relative path to error description (#2) ([`b9e7b6a`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/b9e7b6a4c0bda97b666725f6ba2336c9d2da07c3))

* feat: adds to hook data the file relative path (#2) ([`b7078bb`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/b7078bbbe9b74be1397f90ba09e7b11c08b3b2d5))

### Fix

* fix: adds require serial to prevent running in parallel (#1) ([`a8815b7`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/a8815b77e4242a6522db59d2e302db95861e53a0))

### Test

* test: update test library for a missing member ([`4dbd384`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/4dbd38471b8d8b75c8c5450fb5210335eab3628f))


## v1.0.2 (2024-01-05)

### Fix

* fix: update pypi token variable to work on this project ([`7481102`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/7481102c1d6f423e48ba8844d36fb54e8389eaef))


## v1.0.1 (2024-01-05)

### Ci

* ci: removes major from semantic release, removes unknown from changelog ([`39a647d`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/39a647d5f96de4b199027c09c78abf77519b9129))

* ci: update variable name for pypi ([`eaf526e`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/eaf526e7222f5887c819a0bb4efc19189e01e786))

### Fix

* fix: updates makefile style ([`3ad2e09`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/3ad2e09b7a0315b48cd2e28bd602b7ad28af2e77))


## v1.0.0 (2024-01-05)

### Ci

* ci: updates semantic-release ([`db81784`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/db817843b67402294c9f38a3a098f1e6f5109f2a))

### Documentation

* docs: update readme file ([`9e118af`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/9e118af098e2be55f3a97c97e6cccad437a040a0))

### Feature

* feat: initial commit for v1 of the hooks ([`154ed9b`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/154ed9b9a88ed05dc7e9c6f57739aed4c0849a5d))


## v0.1.0 (2023-12-18)

### Unknown

* Update file README.adoc ([`d7b5ba4`](https://gitlab.com/felipe_public/pre-commit-hooks-g/-/commit/d7b5ba43c0f6bb1bf8193426daff930e0259c5bc))

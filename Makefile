
.PHONY: all format tests setup clean release static-tests unit-tests build-test precommit

# Variables for colors
BYELLOW = "\033[1;33m"
NC =  "\033[0m"
PRUN = poetry run

# Targets
all: format tests

setup:
	@echo -e ${BYELLOW}"Setting up environment and creating VENV..."${NC}
	poetry install
	@echo -e ${BYELLOW}"Installing pre-commit..."${NC}
	${PRUN} pre-commit install
	poetry shell

format:
	@echo -e ${BYELLOW}"# Formatting Python code with black and isort..."${NC}
	${PRUN} black .
	${PRUN} isort . --profile black

tests: clean static-tests unit-tests build-test

static-tests:
	@echo -e ${BYELLOW}"# Running static code analysis tools..."${NC}
	${PRUN} isort . --check
	${PRUN} black --check .
	${PRUN} flake8 --max-line-length=88 --extend-exclude .venv,.mypy_cache .
	${PRUN} mypy .
	${PRUN} pydocstyle
	${PRUN} pylint --max-line-length=88 --exit-zero pre_commit_hooks_g/ tests/python/

unit-tests:
	@echo -e ${BYELLOW}"# Running unit tests..."${NC}
	${PRUN} python -m unittest discover -s tests/ -p '*.py' -q

build-test:
	@echo -e ${BYELLOW}"# Running build test..."${NC}
	poetry build
	pip install dist/*.whl --force-reinstall
	g-hooks -h

clean:
	@echo -e ${BYELLOW}"Cleaning..."${NC}
	find . -type d -name  "__pycache__" -exec rm -r {} +
	rm -rf dist/ .mypy_cache/

release:
	@echo -e ${BYELLOW}"Running release steps..."${NC}
	${PRUN} semantic-release version
	poetry publish

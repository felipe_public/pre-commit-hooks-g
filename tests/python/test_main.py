"""Unit test for the main.py from pre_commit_hooks_g."""
import unittest
from pathlib import Path

from pre_commit_hooks_g.main import VIFileNames, build_gcli_cmd_line, parse_arguments


class TestParseArguments(unittest.TestCase):
    """Tests for the parse arguments function."""

    def test_parse_arguments_vi(self) -> None:
        """Test if the argument --vi, -v is parsed."""
        test_data = ["--vi", "hook.vi"]
        args = parse_arguments(test_data)
        expected_result = "hook.vi"
        result = args.vi
        self.assertEqual(result, expected_result)

    def test_parse_arguments_dialog(self) -> None:
        """Test if the argument --allow-dialogs is parsed."""
        test_data = ["--allow-dialogs", "-v", "hook.vi"]
        args = parse_arguments(test_data)
        result = args.dialogs
        self.assertTrue(result)

    def test_parse_arguments_debug(self) -> None:
        """Test if the argument debug is parsed."""
        test_data = ["--debug", "-v", "hook.vi"]
        args = parse_arguments(test_data)
        result = args.debug
        self.assertTrue(result)

    def test_parse_arguments_filenames(self) -> None:
        """Test if the argument filenames is parsed."""
        test_data = ["-v", "hook.vi", "file1", "file2"]
        args = parse_arguments(test_data)
        expected_result = ["file1", "file2"]
        result = args.filenames
        self.assertEqual(result, expected_result)

    def test_parse_arguments_hooks_args(self) -> None:
        """Test if the argument hook-args is parsed."""
        test_data = [
            "-v",
            "hook.vi",
            "--hook-args",
            "argument1",
            "yes",
            "--",
            "file1",
            "file2",
        ]
        args = parse_arguments(test_data)
        expected_result = ["argument1", "yes"]
        result = args.hook_args
        self.assertEqual(result, expected_result)


class TestBuildCmdLine(unittest.TestCase):
    """Tests for the build g-cli command line function."""

    def setUp(self) -> None:
        """Setup method for tests TestBuildCmdLine."""
        self.wrapper_vi = "wrapper.vi"
        self.hook_vi = "test.vi"
        self.test_file = ["evaluate.vi"]
        self.test_multiple_files = ["evaluate1.vi", "evaluate2.vi"]

    def test_build_cmd_line(self) -> None:
        """Test if the minimal command line is build correctly."""
        expected_result = [
            "g-cli",
            self.wrapper_vi,
            "--",
            "-hook-path",
            self.hook_vi,
            "-files",
            "evaluate.vi",
        ]
        result = build_gcli_cmd_line(
            wrapper_vi=self.wrapper_vi, hook_vi=self.hook_vi, filenames=self.test_file
        )
        self.assertEqual(result, expected_result)

    def test_build_cmd_line_dialogs(self) -> None:
        """Test if the command line with allow-dialogs is build correctly."""
        dialogs = True
        expected_result = [
            "g-cli",
            "--allow-dialogs",
            self.wrapper_vi,
            "--",
            "-hook-path",
            self.hook_vi,
            "-files",
            "evaluate.vi",
        ]
        result = build_gcli_cmd_line(
            wrapper_vi=self.wrapper_vi,
            hook_vi=self.hook_vi,
            allow_dialogs=dialogs,
            filenames=self.test_file,
        )
        self.assertEqual(result, expected_result)

    def test_build_cmd_line_extra_args(self) -> None:
        """Test if the command line with extra-args to hook is build correctly."""
        extra_args = ["args1", "value1", "args2"]
        expected_result = [
            "g-cli",
            self.wrapper_vi,
            "--",
            "-hook-path",
            self.hook_vi,
            "-files",
            "evaluate.vi",
            *extra_args,
        ]
        result = build_gcli_cmd_line(
            wrapper_vi=self.wrapper_vi,
            hook_vi=self.hook_vi,
            filenames=self.test_file,
            extra_args=extra_args,
        )
        self.assertEqual(result, expected_result)

    def test_build_cmd_line_debug(self) -> None:
        """Test if the command line with debug is build correctly."""
        debug = True
        expected_result = [
            "g-cli",
            self.wrapper_vi,
            "--",
            "-debug",
            "-hook-path",
            self.hook_vi,
            "-files",
            "evaluate.vi",
        ]
        result = build_gcli_cmd_line(
            wrapper_vi=self.wrapper_vi,
            hook_vi=self.hook_vi,
            debug=debug,
            filenames=self.test_file,
        )
        self.assertEqual(result, expected_result)

    def test_build_cmd_line_multiple_files(self) -> None:
        """Test if the minimal command line is build correctly when using
        multiple files."""
        expected_result = [
            "g-cli",
            self.wrapper_vi,
            "--",
            "-hook-path",
            self.hook_vi,
            "-files",
            "evaluate1.vi,evaluate2.vi",
        ]
        result = build_gcli_cmd_line(
            wrapper_vi=self.wrapper_vi,
            hook_vi=self.hook_vi,
            filenames=self.test_multiple_files,
        )
        self.assertEqual(result, expected_result)


class TestBuildFileNames(unittest.TestCase):
    """Tests for class VIFileNames"""

    def setUp(self) -> None:
        """Setup method for tests TestBuildFileNames."""
        self.test_class = VIFileNames()

    def test_build_wrapper_vi_path(self) -> None:
        """Test to confirm the path build for wrapper VI."""
        expected_result = (
            Path(__file__).parent.parent.parent
            / "pre_commit_hooks_g"
            / "hooks-wrapper"
            / "hooks-wrapper.vi"
        )
        result = self.test_class.wrapper_vi_path()
        self.assertEqual(Path(result), expected_result)

    def test_build_hook_vi_path(self) -> None:
        """Test to confirm the path build for hook VI."""
        hook_vi_name = "test.vi"
        expected_result = (
            Path(__file__).parent.parent.parent
            / "pre_commit_hooks_g"
            / "g-hooks"
            / hook_vi_name
        )
        result = self.test_class.hook_vi_path(hook_vi=hook_vi_name)
        self.assertEqual(Path(result), expected_result)


if __name__ == "__main__":
    print(Path(__file__).parent.parent)
    unittest.main()

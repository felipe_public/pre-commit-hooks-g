variables:
    POETRY_CACHE_DIR: "$CI_PROJECT_DIR/.cache/poetry"
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

stages:
  - dependencies
  - static analysis
  - testing
  - release

default:
  image: python:3.11.6-slim-bullseye
  cache:
    key: ${CI_PROJECT_PATH} 
    paths:
      - .cache/
  before_script:
    - python -V
    - pip install --quiet poetry && poetry -V
    - poetry install

fetch dependencies:
  stage: dependencies
  script:
    - echo "Downloading and caching dependencies for next jobs..."

# Static Analysis
pydocstyle:
  stage: static analysis
  needs:
    - job: fetch dependencies
      artifacts: false
  script:
    - poetry run pydocstyle

mypy:
  stage: static analysis
  needs:
    - job: fetch dependencies
      artifacts: false
  script:
    - poetry run mypy .

flake8+docstrings:
  stage: static analysis
  needs:
    - job: fetch dependencies
      artifacts: false
  script:
    - poetry run flake8 --verbose --max-line-length=88 pre_commit_hooks_g/ tests/

pylint:
  stage: static analysis
  needs:
    - job: fetch dependencies
      artifacts: false
  script:
    - poetry run pylint --max-line-length=88 --exit-zero pre_commit_hooks_g/ tests/ | tee /tmp/pylint.txt
    - poetry run pylint --max-line-length=88 --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter pre_commit_hooks_g/ tests/ > codeclimate.json
  after_script: 
    - score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt) && echo "Pylint score was $score"
    - mkdir -p public/badges && poetry run anybadge --value=$score --file=public/badges/pylint.svg pylint # pylint badge
  artifacts:
    paths:
      - public/badges/pylint.svg
    reports:
      codequality: codeclimate.json
    when: always

isort:
  stage: static analysis
  needs:
    - job: fetch dependencies
      artifacts: false
  script:
  - poetry run isort pre_commit_hooks_g/ tests/ --check

# Testing
unittest:
  stage: testing
  variables:
    CI_JUNIT_REPORT: tests/report.xml
  script:
    - poetry run python -m xmlrunner discover -s tests/ -p "*.py" --output-file ${CI_JUNIT_REPORT}
  artifacts:
    when: always
    reports:
      junit: ${CI_JUNIT_REPORT}

build test:
  image: python:3.11.6-bullseye # slim version does not have make
  stage: testing
  variables:
    CI_JUNIT_REPORT: tests/report.xml
  needs: 
    - job: unittest
      artifacts: false
  script:
    - make build-test

# Release - Publish
release:
  image: python:3.11.6-bullseye # slim version does not have git
  needs: [unittest, build test]
  variables:
    GITLAB_TOKEN: $PVT_TOKEN
    POETRY_PYPI_TOKEN_PYPI: $PYPI_API_TOKEN
  stage: release
  script:
    - git branch --force $CI_COMMIT_BRANCH $CI_COMMIT_SHA # https://github.com/python-semantic-release/python-semantic-release/issues/701
    - git checkout $CI_COMMIT_BRANCH # https://github.com/python-semantic-release/python-semantic-release/issues/701
    - make release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
